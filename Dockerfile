FROM node:12-alpine as base
FROM base as builder
RUN apk update --no-cache \
    python3 \
    make \
    g++
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install --only=production

FROM base
WORKDIR /usr/src/app

COPY --from=builder /usr/src/app/node_modules ./node_modules
COPY . .
EXPOSE 7000
ENTRYPOINT ["node", "server.js"]

